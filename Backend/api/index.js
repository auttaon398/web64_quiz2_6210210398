const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection =mysql.createConnection({
    host : 'localhost',
    user : 'user',
    password : 'user',
    database : 'RunningSystem'
})

connection.connect()
const express = require('express')
const app = express()
const port = 4000

/* Middleware for Authenicating User Token */
function authenticateToken(req,res,next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token,TOKEN_SECRET, (err,user) => {
        if(err) { return res.sendStatus(403) }
        else{
            req.user = user
            next()
        }
    })
}

// API for Processing User Authorization
app.post("/login", (req,res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query =`SELECT * FROM User WHERE Username='${username}'`
    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error inserting datainto db"
                    })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err,result) => {
                if (result){
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].User_ID ,
                    }
                    console.log(payload)
                    let token = jwt.sign(payload,TOKEN_SECRET,{ expiresIn : '1d'})
                    res.send(token)
                }else { res.send("Tnvalid username/password")}
            })
        }
    })
})

/*Query 1 : list all registrations*/
app.get("/list_registrations", authenticateToken, (req,res) => {
    let query =`SELECT
                User.User_ID ,
                User.U_Name,
                User.U_Surname,
                Product.Pro_name,
                Product.Pro_pince,
                Registration.Re_score,
                Product.Pro_pince,
                Product.Pro_type,
                Product.Pro_unit
            FROM
                User,
                Registration,
                Product
            WHERE
                (Registration.Re_ID = User.User_ID ) 
            AND
                (Registration.Pro_ID = Registration.Pro_ID)`
        connection.query( query, (err,rows) => {
            if(err){
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
            }else{
                res.json(rows)
            }
        })
})

/*Query 2 : list all registrations by running event */   
app.get("/list_reg_product", authenticateToken, (req,res) => {

    let pro_id = req.user.user_id
    let query = `SELECT
                    User.User_ID,
                    User.U_Name,
                    User.U_Surname,
                    Product.Pro_name,
                    Product.Pro_pince,
                    Registration.Re_score,
                    Product.Pro_pince,
                    Product.Pro_type,
                    Product.Pro_unit
                FROM
                    User,
                    Registration,
                    Product
                WHERE
                    (Registration.Re_ID = User.User_ID)
                AND
                    (Registration.Pro_ID = Registration.Pro_ID) 
                AND
                    (Registration.Pro_ID = ${pro_id})`
        connection.query( query, (err,rows) => {
            if(err){
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
            }else{
                res.json(rows)
            }
        })
})

//  Query 3 : list all registrations by running id
app.get("/list_reg_user", (req,res) => {

    let u_id = req.query.u_id
    let query = `SELECT
                    User.User_ID,
                    Product.Pro_name,
                    Product.Pro_pince,
                    Registration.Re_score,
                    Product.Pro_type,
                    Product.Pro_unit,
                        Registration.Re_Time
                FROM
                    User,
                    Registration,
                    Product
                WHERE
                    (User.User_ID=Registration.User_ID)
                AND
                    (Registration.Pro_ID = Product.Pro_ID)
                AND
                        (User.U_ID = ${u_id})`
    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
                res.json({
                        "status" : "400",
                        "message" : "Error querying from running db"
                         })
         }else{
                res.json(rows)
         }
    })
})

// API for registering a new product
app.post("/register_product",authenticateToken , (req, res) => {
    
    let user_id = req.user.user_id
    let pro_id = req.query.pro_id
    let re_score = req.query.re_score
    let comment = req.query.comment

    let query= `INSERT INTO Registration (User_ID,Pro_ID,Re_score,Comment,Re_Time) VALUES ( 
                                                                                    '${user_id}', 
                                                                                    '${pro_id}',
                                                                                    '${re_score}',
                                                                                    '${comment}',
                                                                                     NOW())`
    console.log(query)
    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error inserting datainto db"
                     })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding event succesful"
                     })
                 }
        })
})

// API for registering a read product
app.get("/read_register_product", (req,res) => {

    let query = `SELECT * FROM Registration`
    console.log(query)
    connection.query( query, (err,rows) => {
            if(err){
                console.log(err)
                res.json({
                             "status" : "400",
                             "message" : "Error querying from Product db"
                         })
            }else{
                res.json(rows)
            }
    })
})

// API for registering a new User
app.post("/register_user", (req, res) => {

    let u_name     = req.query.u_name
    let u_surname  = req.query.u_surname
    let u_username = req.query.u_username
    let u_password = req.query.u_password
    
    bcrypt.hash(u_password, SALT_ROUNDS,(err,hash) => {
        let query= `INSERT INTO User (U_Name, U_Surname ,Username,Password) VALUES ( 
                                                                                    '${u_name}' , 
                                                                                    '${u_surname}' , 
                                                                                    '${u_username}', 
                                                                                    '${hash}')`
        console.log(query)
    
        connection.query( query, (err,rows) => {
            if(err){
                console.log(err)
                res.json({
                             "status" : "400",
                             "message" : "Error inserting datainto db"
                         })
            }else{
                console.log(query)
                res.json({
                            "status" : "200",
                            "message" : "Adding new user succesful"
                         })
                     }
            })
    })
  
    
})

// => CRUD Operation for Running Table <=
// => Create Product for Running Table <=
app.post("/add_product", (req, res) => {
    
    let pro_name  = req.query.pro_name
    let pro_pince = req.query.pro_pince
    let pro_type  = req.query.pro_type
    let pro_unit  = req.query.pro_unit


    let query= `INSERT INTO Product ( Pro_name ,Pro_pince ,Pro_type,Pro_unit) VALUES (  '${pro_name}' ,
                                                                                                '${pro_pince}',
                                                                                                '${pro_type}',
                                                                                                '${pro_unit}')`
    console.log(query)
    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error inserting datainto db"
                     })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding data succesful"
                     })
                 }
        })
})

// => Read Product for Running Table <=
app.get("/list_product",authenticateToken, (req,res) => {

    let query = `SELECT * FROM Product`;
    
    connection.query( query, (err,rows) => {
            if(err){
                res.json({
                             "status" : "400",
                             "message" : "Error querying from Product db"
                         })
            }else{
                res.json(rows)
            }
    })
})

// => Update Product for Running Table <=
app.post("/update_product", (req, res) => {
    
    let pro_id    = req.query.pro_id
    let pro_name  = req.query.pro_name
    let pro_pince = req.query.pro_pince
    let pro_type  = req.query.pro_type
    let pro_unit  = req.query.pro_unit

    let query= `UPDATE  Product  SET    Pro_name  ='${pro_name}', 
                                        Pro_pince ='${pro_pince}',
                                        Pro_type  ='${pro_type}',
                                        Pro_unit  ='${pro_unit}'
                                        WHERE 
                                        Pro_ID    = ${pro_id}`
    console.log(query)

    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error update record"
                     })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding update succesful"
                     })
                 }
        })
})

// => Delete Product for Running Table <=
app.post("/delete_product", (req, res) => {
    
    let pro_id = req.query.pro_id

    let query= `DELETE FROM Product  WHERE Pro_ID=${pro_id}`
    console.log(query)

    connection.query( query, (err,rows) => {
        if(err){
            console.log(err)
            res.json({
                         "status" : "400",
                         "message" : "Error delete record"
                     })
        }else{
            res.json({
                        "status" : "200",
                        "message" : "Adding delete succesful"
                     })
                 }
        })
})

app.listen(port, () => {
    console.log(`Now stating Running System Backend ${port}`)
})